<?php

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
 		
 		if ($this->session->userdata('nama') || $this->session->userdata('username')){
 			redirect('c_dashboard');
 		}
	}
 
    
	public function index(){
		$this->load->view('halaman_login');
		
		
	}
	

	public function home(){
		$this->load->view('home');
		}

public function chart(){
		$this->load->view('chart');
		}

public function tabel_barang(){
		$this->load->model('model_barang');
		$data['barang'] = $this->model_barang->tabel_barang()->result();
		$this->load->view('tabel_barang',$data);
		}

public function tabel_konsumen(){
		$this->load->model('model_barang');
		$data['konsumen'] = $this->model_barang->tabel_konsumen()->result();
		$this->load->view('tabel_konsumen',$data);
		}


	public function aksi_login(){
		$data=array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
		);
		$query = $this->m_login->aksi_login($data);


		if ($query->num_rows() > 0){

		
			$data_login = $query->row_array();
			$data_sesi = array(
				'nama'		=> $data_login['nama'],
				'username'	=> $data_login['username'],
			);
			$this->session->set_userdata($data_sesi);


			redirect ('c_dashboard');	
		}	else{
			echo "GAGAL LOGIN";
		}
		}
	}